/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_tolower.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/30 00:33:33 by dtitenko          #+#    #+#             */
/*   Updated: 2016/11/30 01:06:59 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static int	ft_isupper(int c)
{
	return (('A' <= c && c <= 'Z'));
}

int			ft_tolower(int c)
{
	return (ft_isupper(c) ? c + 'a' - 'A' : c);
}
