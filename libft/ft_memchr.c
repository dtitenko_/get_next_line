/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memchr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/26 01:15:03 by dtitenko          #+#    #+#             */
/*   Updated: 2016/11/26 01:56:29 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memchr(const void *s, int c, size_t n)
{
	t_uchar	*ss;

	ss = (t_uchar *)s;
	while (n--)
	{
		if (*ss == (t_uchar)c)
			return (ss);
		ss++;
	}
	return (NULL);
}
